//
// Created by Maxim on 14.11.2020.
//

#ifndef TANK_BATTLE_VECTOR_H
#define TANK_BATTLE_VECTOR_H

namespace Core {

    class Vector {
        int x_;
        int y_;

    public:
        Vector();
        Vector(int x, int y);

        int x() const;
        int y() const;

        bool operator==(const Vector &other) const;
        bool operator!=(const Vector &other) const;

        Vector operator+(const Vector &other) const;
    };

}


#endif //TANK_BATTLE_VECTOR_H
