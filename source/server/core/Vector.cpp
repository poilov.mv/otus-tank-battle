//
// Created by Maxim on 14.11.2020.
//

#include "Vector.h"

namespace Core {

    Vector::Vector()
        : x_(0)
        , y_(0) {
    }

    Vector::Vector(int x, int y)
        : x_(x)
        , y_(y) {
    }

    int Vector::x() const {
        return x_;
    }

    int Vector::y() const {
        return y_;
    }

    bool Vector::operator==(const Vector &other) const {
        return x_ == other.x_
            && y_ == other.y_;
    }

    bool Vector::operator!=(const Vector &other) const {
        return !(*this == other);
    }

    Vector Vector::operator+(const Vector &other) const {
        return {x_ + other.x_, y_ + other.y_};
    }

}
