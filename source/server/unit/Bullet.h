//
// Created by Maxim on 14.11.2020.
//

#ifndef TANK_BATTLE_BULLET_H
#define TANK_BATTLE_BULLET_H

#include "server/core/Vector.h"

namespace Unit {

    struct Bullet {
        Core::Vector position;
        Core::Vector velocity;
    };

}

#endif //TANK_BATTLE_BULLET_H
