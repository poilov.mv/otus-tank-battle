//
// Created by Maxim on 13.11.2020.
//

#ifndef TANK_BATTLE_TANK_H
#define TANK_BATTLE_TANK_H

#include "server/core/Vector.h"

namespace Unit {

    struct Tank {
        Core::Vector position;
        Core::Vector velocity;
    };

}

#endif //TANK_BATTLE_TANK_H
