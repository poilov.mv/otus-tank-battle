//
// Created by Maxim on 15.11.2020.
//

#ifndef TANK_BATTLE_CHANGEANGLEVELOCITYCOMMANDADAPTER_H
#define TANK_BATTLE_CHANGEANGLEVELOCITYCOMMANDADAPTER_H

#include "server/core/Vector.h"
#include <memory>

namespace Command {

    template<class T>
    class ChangeAngleVelocityAdapter {
        T adaptee;

    public:
        ChangeAngleVelocityAdapter(T adaptable)
                : adaptee(adaptable) {}

        void setAngleVelocity(const Core::Vector &value) {
            adaptee->angleVelocity = value;
        }
    };

    template<class T>
    static std::shared_ptr <ChangeAngleVelocityAdapter<T>> ChangeAngleVelocityAdapterPtr(T adaptable) {
        return std::make_shared<ChangeAngleVelocityAdapter<T>>(adaptable);
    }

}

#endif //TANK_BATTLE_CHANGEANGLEVELOCITYCOMMANDADAPTER_H
