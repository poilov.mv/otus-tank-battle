//
// Created by Maxim on 14.11.2020.
//

#ifndef TANK_BATTLE_CHANGEVELOCITYCOMMAND_H
#define TANK_BATTLE_CHANGEVELOCITYCOMMAND_H

#include "server/core/Vector.h"

namespace Command {

    template<class T>
    class ChangeVelocity {
        T obj;
        Core::Vector velocity;

    public:
        ChangeVelocity(T object, const Core::Vector &newValue)
                : obj(object)
                , velocity(newValue) {
        }

        void execute() {
            obj->setVelocity(velocity);
        }
    };

    template<class T, class ... Args>
    static ChangeVelocity<T> makeChangeVelocity(T object, Args ... args) {
        return ChangeVelocity<T>(object, args...);
    }

}

#endif //TANK_BATTLE_CHANGEVELOCITYCOMMAND_H
