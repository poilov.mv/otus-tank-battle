//
// Created by Maxim on 15.11.2020.
//

#ifndef TANK_BATTLE_CHANGEANGLEVELOCITYCOMMAND_H
#define TANK_BATTLE_CHANGEANGLEVELOCITYCOMMAND_H

#include "server/core/Vector.h"

namespace Command {

    template<class T>
    class ChangeAngleVelocity {
        T obj;
        Core::Vector angleVelocity;

    public:
        ChangeAngleVelocity(T object, const Core::Vector &newValue)
                : obj(object)
                , angleVelocity(newValue) {
        }

        void execute() {
            obj->setAngleVelocity(angleVelocity);
        }
    };

    template<class T, class ... Args>
    static ChangeAngleVelocity<T> makeChangeAngleVelocity(T object, Args ... args) {
        return ChangeAngleVelocity<T>(object, args...);
    }

}

#endif //TANK_BATTLE_CHANGEANGLEVELOCITYCOMMAND_H
