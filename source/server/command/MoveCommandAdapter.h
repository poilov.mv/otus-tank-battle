//
// Created by Maxim on 14.11.2020.
//

#ifndef TANK_BATTLE_MOVECOMMANDADAPTER_H
#define TANK_BATTLE_MOVECOMMANDADAPTER_H

#include "server/core/Vector.h"
#include <memory>

namespace Command {

    template<class T>
    class MoveAdapter {
        T adaptee;

    public:
        MoveAdapter(T adaptable)
                : adaptee(adaptable) {}

        void setPosition(const Core::Vector &value) {
            adaptee->position = value;
        }

        Core::Vector getPosition() const {
            return adaptee->position;
        }

        Core::Vector getVelocity() const {
            return adaptee->velocity;
        }
    };

    template<class T>
    static std::shared_ptr <MoveAdapter<T>> MoveAdapterPtr(T adaptable) {
        return std::make_shared<MoveAdapter<T>>(adaptable);
    }

}

#endif //TANK_BATTLE_MOVECOMMANDADAPTER_H
