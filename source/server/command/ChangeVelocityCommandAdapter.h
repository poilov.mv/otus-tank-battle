//
// Created by Maxim on 15.11.2020.
//

#ifndef TANK_BATTLE_CHANGEVELOCITYCOMMANDADAPTER_H
#define TANK_BATTLE_CHANGEVELOCITYCOMMANDADAPTER_H

#include "server/core/Vector.h"
#include <memory>

namespace Command {

    template<class T>
    class ChangeVelocityAdapter {
        T adaptee;

    public:
        ChangeVelocityAdapter(T adaptable)
                : adaptee(adaptable) {}

        void setVelocity(const Core::Vector &value) {
            adaptee->velocity = value;
        }
    };

    template<class T>
    static std::shared_ptr <ChangeVelocityAdapter<T>> ChangeVelocityAdapterPtr(T adaptable) {
        return std::make_shared<ChangeVelocityAdapter<T>>(adaptable);
    }

}

#endif //TANK_BATTLE_CHANGEVELOCITYCOMMANDADAPTER_H
