//
// Created by Maxim on 15.11.2020.
//

#ifndef TANK_BATTLE_EXECUTEPENDINGCOMMANDSCOMMAND_H
#define TANK_BATTLE_EXECUTEPENDINGCOMMANDSCOMMAND_H

namespace Command {

    template<class T>
    class ExecutePendingCommands {
        T obj;

    public:
        ExecutePendingCommands(T object)
                : obj(object) {
        }

        void execute() {
            for(auto command : obj->pendingCommands()) {
                obj->executeCommand(command);
            }
        }
    };

    template<class T>
    static ExecutePendingCommands<T> makeExecutePendingCommands(T object) {
        return ExecutePendingCommands<T>(object);
    }

}

#endif //TANK_BATTLE_EXECUTEPENDINGCOMMANDSCOMMAND_H
