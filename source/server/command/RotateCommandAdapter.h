//
// Created by Maxim on 14.11.2020.
//

#ifndef TANK_BATTLE_ROTATECOMMANDADAPTER_H
#define TANK_BATTLE_ROTATECOMMANDADAPTER_H

#include "server/core/Vector.h"
#include <memory>

namespace Command {

    template<class T>
    class RotateAdapter {
        T adaptee;

    public:
        RotateAdapter(T adaptable)
                : adaptee(adaptable) {}

        void setDirection(const Core::Vector &value) {
            adaptee->direction = value;
        }

        Core::Vector getDirection() const {
            return adaptee->direction;
        }

        Core::Vector getAngleVelocity() const {
            return adaptee->angleVelocity;
        }
    };

    template<class T>
    static std::shared_ptr <RotateAdapter<T>> RotateAdapterPtr(T adaptable) {
        return std::make_shared<RotateAdapter<T>>(adaptable);
    }

}

#endif //TANK_BATTLE_ROTATECOMMANDADAPTER_H
