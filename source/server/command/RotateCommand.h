//
// Created by Maxim on 14.11.2020.
//

#ifndef TANK_BATTLE_ROTATECOMMAND_H
#define TANK_BATTLE_ROTATECOMMAND_H

namespace Command {

    template<class T>
    class Rotate {
        T obj;

    public:
        Rotate(T rotatable)
                : obj(rotatable) {
        }

        void execute() {
            obj->setDirection(obj->getDirection() + obj->getAngleVelocity());
        }
    };

    template<class T>
    static Rotate<T> makeRotate(T rotatable) {
        return Rotate<T>(rotatable);
    }

}


#endif //TANK_BATTLE_ROTATECOMMAND_H
