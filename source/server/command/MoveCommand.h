//
// Created by Maxim on 14.11.2020.
//

#ifndef TANK_BATTLE_MOVECOMMAND_H
#define TANK_BATTLE_MOVECOMMAND_H

namespace Command {

    template<class T>
    class Move {
        T obj;

    public:
        Move(T moveable)
            : obj(moveable) {
        }

        void execute() {
            obj->setPosition(obj->getPosition() + obj->getVelocity());
        }
    };

    template<class T>
    static Move<T> makeMove(T moveable) {
        return Move<T>(moveable);
    }

}

#endif //TANK_BATTLE_MOVECOMMAND_H
