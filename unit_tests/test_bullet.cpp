//
// Created by Maxim on 14.11.2020.
//

#include "gtest/gtest.h"

#include "server/unit/Bullet.h"
#include "server/command/MoveCommandAdapter.h"
#include "server/command/ChangeVelocityCommandAdapter.h"

using namespace Command;
using namespace Unit;

TEST(TestBullet, can_use_bullet_for_move_command_adapter) {
    Bullet bullet;

    auto adapter = MoveAdapterPtr(&bullet);
}

TEST(TestTank, can_use_bullet_with_change_velocity_command) {
    Bullet bullet;

    auto adapter = ChangeVelocityAdapterPtr(&bullet);
}
