//
// Created by Maxim on 14.11.2020.
//

#include "gtest/gtest.h"

#include "server/unit/Tank.h"
#include "server/command/MoveCommandAdapter.h"
#include "server/command/RotateCommandAdapter.h"
#include "server/command/ChangeVelocityCommandAdapter.h"
#include "server/command/ChangeAngleVelocityCommandAdapter.h"

using namespace Command;
using namespace Unit;

TEST(TestTank, can_use_tank_with_move_command) {
    Tank tank;

    auto adapter = MoveAdapterPtr(&tank);
}

TEST(TestTank, can_use_tank_for_rotate_command) {
    Tank tank;

    auto adapter = RotateAdapterPtr(&tank);
}

TEST(TestTank, can_use_tank_with_change_velocity_command) {
    Tank tank;

    auto adapter = ChangeVelocityAdapterPtr(&tank);
}

TEST(TestTank, can_use_tank_with_change_angle_velocity_command) {
    Tank tank;

    auto adapter = ChangeAngleVelocityAdapterPtr(&tank);
}
