//
// Created by Maxim on 15.11.2020.
//

#include "gtest/gtest.h"

#include "server/core/Vector.h"
#include "server/command/ChangeAngleVelocityCommand.h"

using namespace Command;
using namespace Core;

struct AngleVelocityChangableMock {
    Core::Vector angleVelocity;

    void setAngleVelocity(const Core::Vector &value) {
        angleVelocity = value;
    }
};

TEST(TestChangeAngleVelocityCommand, can_change_angle_velocity_on_execute) {
    AngleVelocityChangableMock mock;
    mock.angleVelocity = {1, 2};

    auto command = makeChangeAngleVelocity(&mock, Vector{7, 4});
    command.execute();

    ASSERT_EQ(Vector(7, 4), mock.angleVelocity);
}

TEST(TestChangeAngleVelocityCommand, dont_change_angle_velocity_without_execute) {
    AngleVelocityChangableMock mock;
    mock.angleVelocity = {1, 2};

    auto command = makeChangeAngleVelocity(&mock, Vector{5, 3});

    ASSERT_EQ(Vector(1, 2), mock.angleVelocity);
}

TEST(TestChangeAngleVelocityCommand, can_change_angle_velocity_on_execute_with_another_input) {
    AngleVelocityChangableMock mock;
    mock.angleVelocity = {-3, 4};

    auto command = makeChangeAngleVelocity(&mock, Vector{4, -1});
    command.execute();

    ASSERT_EQ(Vector(4, -1), mock.angleVelocity);
}
