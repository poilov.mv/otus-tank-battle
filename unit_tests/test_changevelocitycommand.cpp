//
// Created by Maxim on 14.11.2020.
//

#include "gtest/gtest.h"

#include "server/core/Vector.h"
#include "server/command/ChangeVelocityCommand.h"

using namespace Command;
using namespace Core;

struct VelocityChangableMock {
    Core::Vector velocity;

    void setVelocity(const Core::Vector &value) {
        velocity = value;
    }
};

TEST(TestChangeVelocityCommand, can_change_velocity_on_execute) {
    VelocityChangableMock mock;
    mock.velocity = {1, 2};

    auto command = makeChangeVelocity(&mock, Vector{7, 4});
    command.execute();

    ASSERT_EQ(Vector(7, 4), mock.velocity);
}

TEST(TestChangeVelocityCommand, dont_change_velocity_without_execute) {
    VelocityChangableMock mock;
    mock.velocity = {1, 2};

    auto command = makeChangeVelocity(&mock, Vector{5, 3});

    ASSERT_EQ(Vector(1, 2), mock.velocity);
}

TEST(TestChangeVelocityCommand, can_change_velocity_on_execute_with_another_input) {
    VelocityChangableMock mock;
    mock.velocity = {-3, 4};

    auto command = makeChangeVelocity(&mock, Vector{4, -1});
    command.execute();

    ASSERT_EQ(Vector(4, -1), mock.velocity);
}
