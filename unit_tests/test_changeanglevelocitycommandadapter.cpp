//
// Created by Maxim on 15.11.2020.
//

#include "gtest/gtest.h"

#include "server/core/Vector.h"
#include "server/command/ChangeAngleVelocityCommand.h"
#include "server/command/ChangeAngleVelocityCommandAdapter.h"

using namespace Command;
using namespace Core;

TEST(TestChangeAngleVelocityCommandAdapter, can_use_field_adapter_for_change_velocity_command) {
    struct Airplane {
        Vector angleVelocity = {1, 8};
    } airplane;

    auto command = makeChangeAngleVelocity(ChangeAngleVelocityAdapterPtr(&airplane), Vector{3, -8});
    command.execute();

    ASSERT_EQ(Vector(3, -8), airplane.angleVelocity);
}

TEST(TestChangeAngleVelocityCommandAdapter, can_use_any_valid_struct_for_change_velocity_command_adapter) {
    struct AnyClassWithAngleVelocity {
        Vector angleVelocity;
    } validObject;

    auto adapter = ChangeAngleVelocityAdapterPtr(&validObject);
}
