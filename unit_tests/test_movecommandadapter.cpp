//
// Created by Maxim on 14.11.2020.
//

#include "gtest/gtest.h"

#include "server/core/Vector.h"
#include "server/command/MoveCommand.h"
#include "server/command/MoveCommandAdapter.h"

using namespace Command;
using namespace Core;

TEST(TestMoveCommandAdapter, can_use_field_adapter_for_move_command) {
    struct Airplane {
        Vector position = {7, 2};
        Vector velocity = {1, 8};
    } airplane;

    auto moveCommand = makeMove(MoveAdapterPtr(&airplane));
    moveCommand.execute();

    ASSERT_EQ(Vector(8, 10), airplane.position);
}

TEST(TestMoveCommandAdapter, can_use_any_valid_struct_for_move_command_adapter) {
    struct AnyClassWithPositionAndVelocity {
        Vector position;
        Vector velocity;
    } validObject;

    auto adapter = MoveAdapterPtr(&validObject);
}
