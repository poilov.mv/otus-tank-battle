//
// Created by Maxim on 15.11.2020.
//

#include "gtest/gtest.h"

#include "server/core/Vector.h"
#include "server/command/ChangeVelocityCommand.h"
#include "server/command/ChangeVelocityCommandAdapter.h"

using namespace Command;
using namespace Core;

TEST(TestChangeVelocityCommandAdapter, can_use_field_adapter_for_change_velocity_command) {
    struct Airplane {
        Vector velocity = {1, 8};
    } airplane;

    auto command = makeChangeVelocity(ChangeVelocityAdapterPtr(&airplane), Vector{3, -8});
    command.execute();

    ASSERT_EQ(Vector(3, -8), airplane.velocity);
}

TEST(TestChangeVelocityCommandAdapter, can_use_any_valid_struct_for_change_velocity_command_adapter) {
    struct AnyClassWithVelocity {
        Vector velocity;
    } validObject;

    auto adapter = ChangeVelocityAdapterPtr(&validObject);
}
