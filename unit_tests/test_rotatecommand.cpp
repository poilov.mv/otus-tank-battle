//
// Created by Maxim on 14.11.2020.
//

#include "gtest/gtest.h"

#include "server/core/Vector.h"
#include "server/command/RotateCommand.h"

using namespace Command;
using namespace Core;

struct RotableMock {
    Core::Vector direction;
    Core::Vector angleVelocity;

    void setDirection(const Core::Vector &value) {
        direction = value;
    }

    Core::Vector getDirection() const {
        return direction;
    }

    Core::Vector getAngleVelocity() const {
        return angleVelocity;
    }
};

TEST(TestRotateCommand, can_change_direction_on_execute) {
    RotableMock rotable;
    rotable.direction = {0, 0};
    rotable.angleVelocity = {1, 2};

    auto moveCommand = makeRotate(&rotable);
    moveCommand.execute();

    ASSERT_EQ(Vector(1, 2), rotable.direction);
}

TEST(TestRotateCommand, dont_change_direction_without_execute) {
    RotableMock rotable;
    rotable.direction = {0, 0};
    rotable.angleVelocity = {1, 2};

    auto moveCommand = makeRotate(&rotable);

    ASSERT_EQ(Vector(0, 0), rotable.direction);
}

TEST(TestRotateCommand, can_change_direction_on_execute_with_another_input) {
    RotableMock rotable;
    rotable.direction = {5, 6};
    rotable.angleVelocity = {-3, 4};

    auto moveCommand = makeRotate(&rotable);
    moveCommand.execute();

    ASSERT_EQ(Vector(2, 10), rotable.direction);
}
