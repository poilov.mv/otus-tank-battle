//
// Created by Maxim on 14.11.2020.
//

#include "gtest/gtest.h"

#include "server/core/Vector.h"
#include "server/command/MoveCommand.h"

using namespace Command;
using namespace Core;

struct MovableMock {
    Core::Vector position;
    Core::Vector velocity;

    void setPosition(const Core::Vector &value) {
        position = value;
    }

    Core::Vector getPosition() const {
        return position;
    }

    Core::Vector getVelocity() const {
        return velocity;
    }
};

TEST(TestMoveCommand, can_change_position_on_execute) {
    MovableMock movable;
    movable.position = {0, 0};
    movable.velocity = {1, 2};

    auto moveCommand = makeMove(&movable);
    moveCommand.execute();

    ASSERT_EQ(Vector(1, 2), movable.position);
}

TEST(TestMoveCommand, dont_change_position_without_execute) {
    MovableMock movable;
    movable.position = {0, 0};
    movable.velocity = {1, 2};

    auto moveCommand = makeMove(&movable);

    ASSERT_EQ(Vector(0, 0), movable.position);
}

TEST(TestMoveCommand, can_change_position_on_execute_with_another_input) {
    MovableMock movable;
    movable.position = {5, 6};
    movable.velocity = {-3, 4};

    auto moveCommand = makeMove(&movable);
    moveCommand.execute();

    ASSERT_EQ(Vector(2, 10), movable.position);
}
