//
// Created by Maxim on 14.11.2020.
//

#include "gtest/gtest.h"

#include "server/core/Vector.h"

using namespace Core;

TEST(TestVector, can_create_empty_vector) {
    Vector vector;

    ASSERT_EQ(0, vector.x());
    ASSERT_EQ(0, vector.y());
}

TEST(TestVector, can_create_vector) {
    Vector vector{1, 2};

    ASSERT_EQ(1, vector.x());
    ASSERT_EQ(2, vector.y());
}

TEST(TestVector, can_create_another_vector) {
    Vector vector{-5, 8};

    ASSERT_EQ(-5, vector.x());
    ASSERT_EQ(8, vector.y());
}

TEST(TestVector, can_compare_vector_on_equality) {
    Vector vector{-5, 8};
    Vector anotherVector{37, 14};

    ASSERT_EQ(vector, vector);
    ASSERT_NE(vector, anotherVector);
}

TEST(TestVector, can_compare_vector_on_equality_with_same_x) {
    Vector vector{11, 5};
    Vector anotherVector{11, -3};

    ASSERT_EQ(vector, vector);
    ASSERT_NE(vector, anotherVector);
}

TEST(TestVector, can_compare_vector_on_equality_with_same_y) {
    Vector vector{-7, 11};
    Vector anotherVector{64, 11};

    ASSERT_EQ(vector, vector);
    ASSERT_NE(vector, anotherVector);
}

TEST(TestVector, can_add_one_vector_to_another) {
    Vector vector{5, 7};
    Vector anotherVector{10, -3};

    Vector result = vector + anotherVector;

    ASSERT_EQ(Vector(15, 4), result);
}

TEST(TestVector, can_add_one_vector_to_another_one_more) {
    Vector vector{13, 2};
    Vector anotherVector{-6, 8};

    Vector result = vector + anotherVector;

    ASSERT_EQ(Vector(7, 10), result);
}
