//
// Created by Maxim on 15.11.2020.
//

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "server/command/ExecutePendingCommandsCommand.h"

using ::testing::Return;
using ::testing::Sequence;

class CommandSourceMock {
public:
    MOCK_METHOD(std::vector<int>, pendingCommands, ());
    MOCK_METHOD(void, executeCommand, (int));
};

using namespace Command;

class TestExecutePendingCommandsCommand : public ::testing::Test {
protected:
    CommandSourceMock commandSource;
};

TEST_F(TestExecutePendingCommandsCommand, dont_fetch_commands_without_execute) {
    EXPECT_CALL(commandSource, pendingCommands)
        .Times(0);

    auto command = makeExecutePendingCommands(&commandSource);
}

TEST_F(TestExecutePendingCommandsCommand, fetch_commands_on_execute) {
    EXPECT_CALL(commandSource, pendingCommands)
            .Times(1)
            .WillOnce(Return(std::vector<int>()));

    auto command = makeExecutePendingCommands(&commandSource);
    command.execute();
}

TEST_F(TestExecutePendingCommandsCommand, can_execute_one_command_in_sequence) {
    EXPECT_CALL(commandSource, executeCommand(5))
        .Times(1);

    ON_CALL(commandSource, pendingCommands)
        .WillByDefault(Return(std::vector<int>({5})));

    auto command = makeExecutePendingCommands(&commandSource);
    command.execute();
}

TEST_F(TestExecutePendingCommandsCommand, can_execute_several_commands_in_sequence) {
    EXPECT_CALL(commandSource, executeCommand)
        .Times(2);

    ON_CALL(commandSource, pendingCommands)
        .WillByDefault(Return(std::vector<int>({4, 8})));

    auto command = makeExecutePendingCommands(&commandSource);
    command.execute();
}

TEST_F(TestExecutePendingCommandsCommand, can_execute_several_commands_in_fifo_sequence) {
    Sequence sequence;

    EXPECT_CALL(commandSource, executeCommand(8))
        .InSequence(sequence);
    EXPECT_CALL(commandSource, executeCommand(1))
        .InSequence(sequence);

    ON_CALL(commandSource, pendingCommands)
        .WillByDefault(Return(std::vector<int>({8, 1})));

    auto command = makeExecutePendingCommands(&commandSource);
    command.execute();
}
