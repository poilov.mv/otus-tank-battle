//
// Created by Maxim on 14.11.2020.
//

#include "gtest/gtest.h"

#include "server/core/Vector.h"
#include "server/command/RotateCommand.h"
#include "server/command/RotateCommandAdapter.h"

using namespace Command;
using namespace Core;

TEST(TestRotateCommandAdapter, can_use_field_adapter_for_rotate_command) {
    struct Airplane {
        Vector direction = {7, 2};
        Vector angleVelocity = {1, 8};
    } airplane;

    auto rotateCommand = makeRotate(RotateAdapterPtr(&airplane));
    rotateCommand.execute();

    ASSERT_EQ(Vector(8, 10), airplane.direction);
}

TEST(TestRotateCommandAdapter, can_use_any_valid_struct_for_rotate_command_adapter) {
    struct AnyClassWithDirectionAndAngleVelocity {
        Vector direction;
        Vector angleVelocity;
    } validObject;

    auto adapter = RotateAdapterPtr(&validObject);
}
